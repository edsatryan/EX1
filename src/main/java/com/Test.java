package com;

import org.apache.log4j.Logger;

public class Test {
    final static Logger logger = Logger.getLogger(Test.class);

    public static void main(String args[]) {
        System.out.println("Hello Maven");

        logger.error("SORRY, something wrong!");
        logger.info("INFO, something wrong!");
    }
}
